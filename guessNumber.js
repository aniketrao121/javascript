function guessingGame(num){
    var count = 1;
    while(true){
    var user_guess = prompt("Guess a number between 0 to 100");
    // ---------------- Error checks ---------------//
    if(user_guess.indexOf(".") != -1){
        alert("Please enter a whole number. Thanks!");
        count++;
        continue;
    }
    user_guess = parseInt(user_guess);
    if (isNaN(user_guess)){
        alert("Please enter a valid number");
        count++;
        continue;
    }
        
    // ---------------- Error checks end ---------------//
    if(user_guess > 100){
        alert("Please enter a number between the range of 0 to 100.");
        count++;
        continue;
    }
    if(user_guess > num){
        alert("Please enter a lower number!");
        count++;
    }
    else if(user_guess < num){
        alert("Please enter a higher number!");
        count++;
    }
    else{
        alert("You got it right after " + count + " guesses. Good job!");
        break;
    }
    }
    
    document.write("Thanks for playing! See you soon.")
}


//generated a random number - range [0,100]
guessingGame(Math.floor(Math.random() * 101)); 