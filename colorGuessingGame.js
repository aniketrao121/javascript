function colorGuessingGame(){
    var arr = ["blue","green","yellow","orange","pink","purple"];
    var count = 1;
    var position = Math.floor(Math.random() * arr.length);
    position = parseInt(position);
    while(true){
    var user_guess = prompt("Guess the color from the following colors:"
                           +"\n"
                           +arr.join());
    // ---------------- Error checks -------------//
    if(isNaN(parseInt(user_guess)) == false){
        alert("Make sure your guess is a string");
        count++;
        continue;
    }
    if(arr.indexOf(user_guess) == -1){
        alert("Please make sure your guess belongs to list.");
        count++;
        continue;
    }
    //-----------Error checks end -------------//
    if(arr[position] == user_guess){
        alert("You got it right in " + count + " guesses!");
        break;
    }
    else{
        alert("Sorry, guess again.");
        count++;
    }
    }
    document.write("Thanks for playing! See you soon.")
    document.body.style.backgroundColor=user_guess;
}

